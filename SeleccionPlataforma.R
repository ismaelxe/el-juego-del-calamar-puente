columnas <- 5


seleccionPlataforma <- function(){
  comprobacion <- FALSE
  
  while (comprobacion == FALSE) {
    entrada <- readline(prompt = paste("Introduce un n?mero entre 1 y",columnas,": "))
    
    if (is.na(as.numeric(entrada))){
      print("Debes introducir un n?mero entero")
    }
    else {
      entrada <- as.integer(entrada)
    }
    if(entrada < 1 || entrada > columnas){
      print(paste("El n?mero introducido debe estar entre 1 y",columnas))
      print("Int?ntelo de nuevo")
    }
    else {
      comprobacion <- TRUE
    }
  }
  return(entrada)
}

game <- function(filas, columnas, numero_jugadores, juego_manual = TRUE){
  plataforma_actual <- 1 #Al empezar estamos en la primera plataforma
  jugador_actual <- 1 # Empezamos a jugar con el primer jugador
  
  puente <- generarPuente(filas, columnas)
  historico <- generarHistorico(numero_jugadores, filas)
  
  
  while(TRUE){
    if(juego_manual){print(paste("Jugador ", jugador_actual, ""))}
    
    
    
    if(juego_manual){
      opcion <- seleccionPlataforma()  
    }else {
      opcion <- rbinom(1, 1, 0.5) + 1  
    }
    
    
    resultado_saltar <- saltar(puente, c(plataforma_actual, (opcion)))
    if(resultado_saltar){
      if(juego_manual){print(paste("Jugador ", jugador_actual, " posición ", plataforma_actual, ""))}
    } else {
      if(juego_manual){print(paste("Jugador ", jugador_actual, " muerto.", ""))}
    }
    historico <- guardar_historico(historico, jugador_actual, plataforma_actual, resultado_saltar)
    if(juego_manual){dibujarPuente(puente, c(plataforma_actual, opcion))}
    
    
    #### Incrementar variables ########
    if(!resultado_saltar){
      jugador_actual <- jugador_actual + 1
    }
    ##### Incrementamos la plataforma ya que solamente hay dos opciones y una se ha escogido
    plataforma_actual <- plataforma_actual + 1
    
    if(numero_jugadores < jugador_actual){
      if(juego_manual){print('TODOS LOS JUGADORES HAN SIDO ELIMINADOS')}
      break;
    }
    if(filas < plataforma_actual){
      historico$Meta[jugador_actual] = 'V'
      if(juego_manual){print(paste('Jugador ', jugador_actual, ' HAS GANADO', ""))}
      break;
    }
    
    
    
  }
  
  if(juego_manual){print("FIN DEL JUEGO")}
  #return(historico)
  historico <- historico %>% mutate(id = row_number())
  id_ganador <- historico %>% filter(Meta == 'V') %>% select(id)
  historico <- historico %>% mutate(sobrevive = (if(count(id_ganador)>0){id >= id_ganador$id}else{FALSE} ) )
  
  return(historico)
  
}